    <?php
class Tumblr{
        public function InputForm(){
    ?>

        <form method="post">
            <input type="text" name="blogname" placeholder="Enter Blog Name">
            <input type="number" name="min" placeholder="Enter the start value" min="0">
            <input type="number" name="max" placeholder="Enter the end value">
            <button type="submit">Submit</button>
        </form>

    <?php
        }
        public function GetData(){
            $BlogName = $_POST['blogname'];
            //connecting to the Tumblr with the specified blog name by the user.
            $URL = "https://$BlogName.tumblr.com/api/read/json";
            $DataFromURL = curl_init($URL);
            curl_setopt($DataFromURL, CURLOPT_RETURNTRANSFER, TRUE);
            $ProcessedData = curl_exec($DataFromURL);
            return $ProcessedData;
            }
        public function CleanData($ProcessedData){
            $ProcessedData = str_replace('var tumblr_api_read = ','',$ProcessedData);
            $ProcessedData = str_replace(';','',$ProcessedData);
            return $ProcessedData;
            }
        public function DataProcessing($ProcessedData){
            $FinalData = json_decode($ProcessedData, true);
            return $FinalData;
        }
        //processing the data returned by the Tumblr
        public function ShowData($FinalData){
            $Max = $_POST['max'];
            $Min = $_POST['min'];
            $Name = $FinalData['tumblelog']['name'];
            $Title = $FinalData['tumblelog']['title'];
            $TotalPosts = $FinalData['posts-total'];
            $Description = $FinalData['tumblelog']['description'];
            $Content = $FinalData['posts'];
            $DataType = 'photo-url-1280';
            echo 'Title:' .$Title.'<br>';
            echo 'Name:' .$Name.'<br>';
            echo 'No. of Posts:'.$TotalPosts.'<br>';
            echo 'Description:'.$Description.'<br>';
            for ($Counter = $Min; $Counter <= $Max; $Counter++) {
                echo($Content[$Counter][$DataType]).'<br>';
                }
            }
    }

    $TumblrLinks = new Tumblr();
    $TumblrLinks->InputForm();
    $ProcessedData = $TumblrLinks->GetData();
    $ProcessedData = $TumblrLinks->CleanData($ProcessedData);
    $FinalData = $TumblrLinks->DataProcessing($ProcessedData);
    $TumblrLinks->ShowData($FinalData);
?>