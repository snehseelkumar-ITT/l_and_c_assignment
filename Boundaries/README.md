To run this application, install the `composer` which is a dependency manager for PHP.

After that we can use the following command to require the PHPUNIT framework.
`composer require --dev phpunit/phpunit ^7`

Also for using the .env file run the following command to require the phpdotenv 
`composer require vlucas/phpdotenv`