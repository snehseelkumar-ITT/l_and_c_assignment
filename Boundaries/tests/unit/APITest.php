<?php
use PHPUnit\Framework\TestCase;
require __DIR__ .'/../../APIHandler.php';
class firstTest extends TestCase{

    /** @test */
    public function withLocationInput1(){
        $apiTest = new APIHandler;
        $this->assertArrayHasKey('latitude', $apiTest->apiDataProcessing('jaipur'));
    }

    /** @test */
    public function withLocationInput2(){
        $apiTest = new APIHandler;
        $this->assertArrayHasKey('latitude', $apiTest->apiDataProcessing('delhi'));
    }

    /** @test */
    public function withLocationInput3(){
        $apiTest = new APIHandler;
        $this->assertArrayHasKey('latitude', $apiTest->apiDataProcessing('new delhi'));
    }

    /** @test */
    public function emptyLocationInput(){
        $apiTest = new APIHandler;
        $this->assertArrayHasKey('error', $apiTest->apiDataProcessing(' '));
    }

    /** @test */
    public function withRandomSpecialChars(){
        $apiTest = new APIHandler;
        $this->assertStringContainsString('Error:' ,$apiTest->apiDataProcessing('!@#$%^&*'));
    }

    /** @test */
    public function withLocationAndIllegalChars(){
        $apiTest = new APIHandler;
        $this->assertArrayHasKey('latitude', $apiTest->apiDataProcessing('!!jaipur'));
    }

    /** @test */
    public function withInvalidLocation(){
        $apiTest = new APIHandler;
        $this->assertArrayHasKey('error' ,$apiTest->apiDataProcessing('dfdgfdgf'));
    }

    /** @test */
    public function checkLatitudeJaipur(){
        $apiTest = new APIHandler;
        $this->assertContains('26.916194', $apiTest->apiDataProcessing('jaipur'));
    }

    /** @test */
    public function checkLongitudeJaipur(){
        $apiTest = new APIHandler;
        $this->assertContains('75.820349', $apiTest->apiDataProcessing('jaipur'));
    }

    /** @test */
    public function checkLatitudeDelhi(){
        $apiTest = new APIHandler;
        $this->assertContains('28.6517178', $apiTest->apiDataProcessing('delhi'));
    }

    /** @test */
    public function checkLongitudeDelhi(){
        $apiTest = new APIHandler;
        $this->assertContains('77.2219388', $apiTest->apiDataProcessing('delhi'));
    }

    /** @test */
    public function checkLatitudeBanglore(){
        $apiTest = new APIHandler;
        $this->assertContains('12.9587464', $apiTest->apiDataProcessing('banglore'));
    }

    /** @test */
    public function checkLongitudeBanglore(){
        $apiTest = new APIHandler;
        $this->assertContains('77.5573456', $apiTest->apiDataProcessing('banglore'));
    }

    /** @test */
    public function checkLatitudeMuzaffarpur(){
        $apiTest = new APIHandler;
        $this->assertContains('26.1486581', $apiTest->apiDataProcessing('muzaffarpur'));
    }

    /** @test */
    public function checkLongitudeMuzaffarpur(){
        $apiTest = new APIHandler;
        $this->assertContains('85.3400128247963', $apiTest->apiDataProcessing('muzaffarpur'));
    }
}