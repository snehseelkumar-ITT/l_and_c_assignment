<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('APIHandler.php');
require('functions.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Get Geocodes</title>
</head>
<body>
<h1 class="text-center mt-5">Get Geodes</h1>
<div class="container text-center">
    <form class="form-group" method="post">
    <input class="form-control" placeholder="Enter a place to search" autocomplete="off" type="text" name="searchValue" required>
    <button type="submit" name="searchLocation" class="btn btn-success mt-2">Get Geocodes</button>
    </form>
</div>
<div class="container showResult">
<?php
if(isset($_POST['searchLocation']) && isset($_POST['searchValue'])){
    $apiDataProcessing = new APIHandler();
    $apiResponse = $apiDataProcessing->apiDataProcessing($_POST['searchValue']);
    $arrayKeyExists = array_key_exists("error", (array)$apiResponse) ? true : false;
    $latitudeKeyExists = array_key_exists("latitude", (array)$apiResponse) && array_key_exists("longitude", $apiResponse) ? true : false;
    if($arrayKeyExists){
    echo($apiResponse['error']);
    }
    else if($latitudeKeyExists){
        echoLocationData($apiResponse);
    }
}
else if(isset($_POST['searchLocation'])){
    echo "Please enter the location.";
}
?>
</div>
</body>
</html>
