<?php
ini_set('display_errors', 1);
class apiException extends Exception {
  public function errorMessage() {
    $errorMsg = "Unable to get GeoCode for ".$this->getMessage();
    return $errorMsg;
  }
}
?>