<?php
function echoLocationData($apiResponse){
    echo "<pre>";
    echo "Latitude: ";
    echo $apiResponse['latitude'];
    echo "\nLongitude: ";
    echo $apiResponse['longitude'];
    echo "\nPlace Name: ";
    echo $apiResponse['placeName'];
}