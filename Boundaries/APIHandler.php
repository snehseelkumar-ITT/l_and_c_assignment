<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require('exceptionClass.php');
require_once realpath(__DIR__."/vendor/autoload.php");

use Dotenv\Dotenv;
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

class APIHandler{

	function callAPI($searchLocation){
		$curl = curl_init();
		$data=array();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $_ENV['APILink'].$_ENV['APIKey'].$_ENV['APIQuery'].$searchLocation.$_ENV['APIReturnFormat'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));
		$response = curl_exec($curl);
		$data=json_decode($response);
		curl_close($curl);
		return $data;
	}

	function apiDataProcessing($searchLocation){
		try{
			$data = $this->callAPI($searchLocation);
			if(!$data){
				throw new apiException($searchLocation);
			}
			else if(array_key_exists("error", (array)$data)){
				$error = array("error"=>$data->error);
				return $error;
			}
			else {
				$response = array(
					"latitude" => $data[0]->lat,
					"longitude" => $data[0]->lon,
					"placeName" => $data[0]->display_name,
				);
				return $response;
			}
		}
		catch(apiException $e){
			return "Error: ".$e->errorMessage();
		}	
	}
}
?>