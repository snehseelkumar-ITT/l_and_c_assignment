Class Employee {
    string name;
    int age;
    float salary;
    public: string getName();
    void setName(string name);
    int getAge();
    void setAge(int age);
    float getSalary();
    void setSalary(float salary);
};
Employee employee;

1. Is 'employee' an object or a data structure ? Why ? 

Solution: employee is a data structure for the following reasons:
	I. There are no specific methods with this class.
	II. This object is just used to store data.
	III. It only sets the values for the specified variables such as name, age, salary and calls the methods for the same as well.
	IV. Can be used to access the data only and set it, but not to perform any other operation on the same.
