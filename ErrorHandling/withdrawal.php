<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
if(!isset($_SESSION['atmCardNumber'])){
    header('Location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Withdrawal</title>
</head>
<body>
    <?php
    require('options.php');
    ?>
    <form action="" class="form-group" method="post">
    Enter the amount to Withdraw:
    <input type="number" required name="withdrawalAmount">
    <button type="submit" name="submit">Withdraw</button>
    </form>
</body>
</html>
<?php
if(isset($_POST['submit'])){
    function withdraw($withdrawalAmount, $atmCardNumber){
        require("dbCon.php");
        $sql = "SELECT atmBalance FROM atmDetails";
        $data = $connectionOBJ->query($sql);
        $row = $data->fetch(PDO::FETCH_ASSOC);
        $atmBalance = $row['atmBalance'];
        $sql = "SELECT accountBalance FROM userDetails WHERE cardNumber = $atmCardNumber";
        $data = $connectionOBJ->query($sql);
        $row = $data->fetch(PDO::FETCH_ASSOC);
        $accountBalance = $row['accountBalance'];
        if($withdrawalAmount > $atmBalance){
            throw new Exception('Not enough balance in ATM, please try with smaller amount');
        }
        if( $withdrawalAmount > $accountBalance){
            throw new Exception('Not enough balance in Account, please try with smaller amount');
        }
        $atmBalance = $atmBalance - $withdrawalAmount;
        $accountBalance = $accountBalance - $withdrawalAmount;
        $sql = "UPDATE atmDetails SET atmBalance = '$atmBalance'";
        $data = $connectionOBJ->query($sql);
        $sql = "UPDATE userDetails SET accountBalance = '$accountBalance' WHERE cardNumber = '$atmCardNumber'";
        $data = $connectionOBJ->query($sql);
        throw new Exception('Collect Cash!');
    }
    try{
        $withdrawalAmount = $_POST['withdrawalAmount'];
        $atmCardNumber = $_SESSION['atmCardNumber'];
        withdraw($withdrawalAmount, $atmCardNumber);
    }
    catch(Exception $e){
        echo "Message: ".$e->getMessage();
    }
}
?>