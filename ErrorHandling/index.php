<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require("dbCon.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>ATM</title>
</head>
<body>
    <h1>ATM</h1>
    <form method="post">
    <input type="text" name="atmCardNumber" autocomplete="off">
    <input type="password" name="atmPIN" autocomplete="off">
    <button type="submit" name="submit">Log In</button>
    </form>
</body>
</html>

<?php
if(isset($_POST['submit'])){
    require('functions/userDetailsFunction.php');
    $atmCardNumber = $_POST['atmCardNumber'];
    $atmPIN = $_POST['atmPIN'];
    $atmDetail = array("atmPIN"=> $atmPIN, "atmCardNumber"=>$atmCardNumber);
    $row = userDetail($atmCardNumber, $connectionOBJ);
    if(checkPINFailedAttempts($atmDetail, $row)){
        header('Location:userInterface.php');
    }
    else if(checkBlockedCard($row)){
        echo "Card Blocked!";
    }
    else{
        $attempts = $row['failedAttempts'];
        $attempts = $attempts - 1;
        $userDetail = array("attempts"=> $attempts, "atmCardNumber"=>$atmCardNumber);
        updateFailedAttempts($userDetail, $connectionOBJ);
    }
}
?>