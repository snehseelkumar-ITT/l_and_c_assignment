<?php
error_reporting(E_ALL);
if(!isset($_SESSION['atmCardNumber'])){
    header('Location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <form method="post">
    <button name="logout" type="submit">Logout</button>
    </form>
    <br>
    <a href="withdrawal.php">Withdraw Cash</a>
    <br>
    <a href="balanceInquiry.php">Balance Inquiry</a>
    <br>
    <a href="pinChange.php">Change ATM PIN</a>
</body>
</html>
<?php
if(isset($_POST['logout'])){
    session_destroy();
    header('Location:index.php');
}
?>