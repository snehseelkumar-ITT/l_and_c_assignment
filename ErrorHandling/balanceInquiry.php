<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
if(!isset($_SESSION['atmCardNumber'])){
    header('Location:index.php');
}
require('options.php');
require("dbCon.php");
require('functions/balanceEnquiryFunction.php');
$atmCardNumber = $_SESSION['atmCardNumber'];
balanceEnquiry($atmCardNumber, $connectionOBJ);