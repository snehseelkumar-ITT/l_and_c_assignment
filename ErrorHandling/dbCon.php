<?php
class connect_db{
    private $servername;
    private $dbname;
    private $password;
    private $username;
    private $charset;
    private $options;
    public function connectdb()
    {
        $this->servername = 'localhost';
        $this->username = 'hello';
        $this->password = "Hello@123";
        $this->dbname = "ATM";
        $this->charset = "utf8mb4";
        try{
            $dsn = 'mysql:dbname='.$this->dbname.'; host='.$this->servername;
            $pdo = new PDO($dsn, $this->username, $this->password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        }
        catch(PDOException $e){
                echo "Can't connect to SERVER". $e->getMessage();
        }
    }
}
try{
$connect = new connect_db;
$connectionOBJ = $connect->connectdb();
if (!$connectionOBJ) {
    throw new Exception('SERVER NOT RESPONDING!');
    }
}
catch(Exception $e){
    echo $e->getMessage();
}