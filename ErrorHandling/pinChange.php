<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
if(!isset($_SESSION['atmCardNumber'])){
    header('Location:index.php');
}
require('options.php');
require("dbCon.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body class="container mt-3">
    <form action="" class="form-group" method="post">
    Enter New ATM PIN twice
    <br>
    <input type="number" class="mb-1" name="pin1" required>
    <br>
    <input type="number" class="mb-1" name="pin2" required>
    <br>
    <button type="submit" name="pinChange">Change ATM PIN</button>
    </form>
</body>
</html>
<?php
if(isset($_POST['pinChange'])){
    if($_POST['pin1'] == $_POST['pin2']){
        $pin = $_POST['pin1'];
        $atmCardNumber = $_SESSION['atmCardNumber'];
        $sql = "UPDATE userDetails SET PIN = '$pin' WHERE cardNumber = '$atmCardNumber'";
        $data = $connectionOBJ->query($sql);
        echo "PIN Changed Sucessfull!";
    }
}