# Introduction

Tumblr is an American microblogging and social networking website founded by David Karp in 2007 and currently owned by Automattic.

The service allows users to post multimedia and other content to a short-form blog.

https://www.tumblr.com/

Tumblr provides API to get info about public blogs. However, there are two versions of API, v1 and v2.

For this assignment, we are going to use API v1.

# Purpose of This project

To fetch the urls of all the images of quality 1280 with the help of API

# Formatting for the code

This is to provide a structure to the code. Follow these specific rules to keep the consistency among the team.

## Spacing between the concepts

There will be a single space line between the concepts.

#### Example

```PHP
function sum( $first_number, $second_number){

return $first_number + $second_number ;
}
```

## Spacing between the operators

There will a single space between the operators

#### Example:

```PHP
$Total = ( $principle + $rate + $time) / 100 ;
```

## Variable Declaration


* Variable should be declare nearby their uses.

* Instance variable should be declare near the respective class.

* Use snake casing in naming of the variable.

## Indentation format and spacing

* 4 space is provided in function defination

* Separation between the formal and actual arguments in function calling

  Provide single space after the argument separator( ***, *** ).

#### Example

```PHP
function calculator( $first_digit, $second_digit){

    $sum = $first_digit + $second_digit ;
    $subtration = $first_digit - $second_digit ;
}

```

## Vertical Alignment

**No Vertical Alignment is required** 

* Its the choice of the user if he/she want to use it.

## Maximum Line Length

* Limit all lines to a maximum of 100 characters.

## Blank Lines

* Method definitions inside a class are surrounded by a single blank line.

#### Example

```php

class Myclass(){

      o-->one line space here
      function myfunction();
}

```

## Includes

* Includes should usually be on separete lines.


#### Example

```PHP
include ( 'calculateSum.php' ) ;
include ( 'calculateDiff.php' ) ;
```

## String Quotes

In PHP, single-quoted strings and double-quoted strings are the same.

When a string contains single or double quote characters, however, use the other one to avoid backslashes in the string. It improves readability.

## Comments

* Comments should be complete sentences. The first word should be capitalized, unless it is an identifier that begins with a lower case letter (never alter the case of identifiers!).

* Comments that contradict the code are worse than no comments. Always make a priority of keeping the comments up-to-date when the code changes!

* Provide space above and below of your comment.

## Naming Conventions

* Snake of naming of variable followed.

* Never use the characters 'l' (lowercase letter el), 'O' (uppercase letter oh), or 'I' (uppercase letter eye) as single character variable names.

#### Package and Module Names

* Modules should have short, all-lowercase names. Underscores can be used in the module name if it improves readability.

#### Class Names

* Class names should normally use the CapWords convention.

#### Function and Variable Names

* Function names should be lowercase, with words separated by underscores to improve readability.(snake_case)


```PHP

```