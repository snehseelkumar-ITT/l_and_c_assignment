<?xml version="1.0"?>
<ruleset name="Custom Standard">

 <!--
    The name attribute of the ruleset tag is displayed when running PHP_CodeSniffer with the -v command line argument. The description tag below is not displayed anywhere except in this file, so it can contain information for developers who may change this file in the future.
 -->
 <description>A custom coding standard</description>

 <!--
    You can hard-code ignore patterns directly into your custom standard so you don't have to specify the patterns on the command line.
    
    The following two tags are equivalent to the command line argument: - -ignore=*/tests/*,*/data/*
 -->
 <exclude-pattern>*/tests/*</exclude-pattern>
 <exclude-pattern>*/data/*</exclude-pattern>

<!-- Including PSR2 sniff and removing the specific rules that are not required. -->

<rule ref="PSR2">
	<exclude name="PSR2.Methods.FunctionClosingBrace"/>
	<exclude name="Squiz.Functions.FunctionDeclaration"/>
	<exclude name="PSR2.Classes.ClassDeclaration.OpenBraceNewLine" />
	<exclude name="Squiz.Functions.MultiLineFunctionDeclaration.BraceOnSameLine" />
</rule>

<!-- Including PSR12 sniff and removing the specific rules that are not required. -->

 <rule ref="PSR12">
  <exclude name="PSR2.Files.ClosingTag"/>
 </rule>

<!-- Including Generic sniff and setting up the required property related to the indentation and its value. -->

<rule ref="Generic.Arrays.ArrayIndent">
    <properties>
        <property name="indent" value="2" />
    </properties>
</rule>
 <!--
    Include some specific sniffs from the Generic standard. Note again that the name of the sniff is the code that PHP_CodeSniffer gives it.
 -->
 <rule ref="Generic.CodeAnalysis.UnusedFunctionParameter"/>
 <rule ref="Generic.Commenting.Todo"/>
 <rule ref="Generic.ControlStructures.InlineControlStructure"/>

 <!--
    Here we are including a specific sniff but also changing the error message of a specific message inside the sniff. Note that the specific code for the message, which is CommentFound in this case, is defined by the sniff developer. You can display these codes by using the -s command line argument when checking a file.

    Also note that this message has a variable inside it, which is why it is important that sniffs use a printf style format for their error messages.

    We also drop the severity of this message from the default value (5) so that it is hidden by default. It can be displayed by setting the minimum severity on the PHP_CodeSniffer command line. This is great if you want to use some messages only in code reviews and not have them block code commits.
 -->
 <rule ref="Generic.Commenting.Todo.CommentFound">
  <message>Please review this TODO comment: %s</message>
  <severity>3</severity>
 </rule>

 <!--
    Here we change two messages from the same sniff. Note how the codes are slightly different because the sniff developer has defined both a MaxExceeded message and a TooLong message. In the case of this sniff, one is used for warnings and one is used for errors.
 -->
 <rule ref="Generic.Files.LineLength.MaxExceeded">
  <message>Line contains %s chars, which is longer than the max limit of %s</message>
 </rule>
 <rule ref="Generic.Files.LineLength.TooLong">
  <message>Line longer than %s characters; contains %s characters</message>
 </rule>

 <!--
    Some sniffs have public member vars that allow you to customise specific elements of the sniff. In the case of the Generic LineLength sniff, you can customise the limit at which the sniff will throw warnings and the limit at which it will throw errors.

    The rule below includes the LineLength sniff but changes the settings so the sniff will show warnings for any line longer than 90 chars and errors for any line longer than 100 chars.
 -->
 <rule ref="Generic.Files.LineLength">
  <properties>
   <property name="lineLimit" value="300"/>
   <property name="absoluteLineLimit" value="300"/>
  </properties>
 </rule>

 <!--
    Another useful example of changing sniff settings is to specify the end of line character that your standard should check for.
 -->
 <rule ref="Generic.Files.LineEndings">
  <properties>
   <property name="eolChar" value="\n"/>
  </properties>
 </rule>

 <!--
    Boolean values should be specified by using the strings "true" and "false" rather than the integers 0 and 1.
 -->
 <rule ref="Generic.Formatting.MultipleStatementAlignment">
  <properties>
   <property name="maxPadding" value="8"/>
   <property name="ignoreMultiLine" value="true"/>
   <property name="error" value="true"/>
  </properties>
 </rule>

 <!--
    If you want to completely disable an error message in a sniff but you don't want to exclude the whole sniff, you can change the severity of the message to 0. In this case, we want the Squiz DoubleQuoteUsage sniff to be included in our standard, but we don't want the ContainsVar error message to ever be displayed.
 -->
 <rule ref="Squiz.Strings.DoubleQuoteUsage.ContainsVar">
  <severity>0</severity>
 </rule>

 <!--
    There is a special internal error message produced by PHP_CodeSniffer when it is unable to detect code in a file, possible due to the use of short open tags even though php.ini disables them. You can disable this message in the same way as sniff messages.

    Again, the code here will be displayed in the PHP_CodeSniffer output when using the -s command line argument while checking a file.
 -->
 <rule ref="Internal.NoCodeFound">
  <severity>0</severity>
 </rule>

</ruleset>
