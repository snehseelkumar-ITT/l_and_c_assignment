<?php
    error_reporting(E_ALL) ;
    require 'getData.php' ;
    require 'dataProcessing.php' ;
    require 'showData.php' ;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tumblr</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>

    <body>
        <div class="container m-2">
            <form method="post" class="form-group form-inline">
                <input type="text" name="blogName" class="m-2 form-control" required placeholder="Enter Blog name">
                <input type="number" name="min" class="m-2 form-control" required placeholder="Enter the start value" min="1">
                <input type="number" name="max" class="m-2 form-control" required placeholder="Enter the end value" min="1">
                <button name="submit" value="submit" id="submit-button" class="btn m-2 btn-primary">Submit</button>
            </form>
        </div>

        <div class="container" id="api-response">

            <?php
            if (isset($_POST['submit'])) {
                $detailsPrinted = 0;
                $min = $_POST['min'];
                $max = $_POST['max'];
                
                for ($count = $min; $count <= $max; $count++) {
                    $processedData = getData();
                    $processedData = cleanData($processedData);
                    $finalData     = dataProcessing($processedData);

                    if ($detailsPrinted == 0) {
                        $name        = $finalData['tumblelog']['name'];
                        $title       = $finalData['tumblelog']['title'];
                        $totalPosts  = $finalData['posts-total'];
                        $description = $finalData['tumblelog']['description'];
                        echo 'title:' . $title . '<br>';
                        echo 'name:' . $name . '<br>';
                        echo 'No. of Posts:' . $totalPosts . '<br>';
                        echo 'description:' . $description . '<br>';
                        $detailsPrinted = 1;
                    }

                        $totalPosts = $finalData['posts-total'];
                        
                    if ($totalPosts < $max) {
                        echo "There are only " . $totalPosts . "Posts. Please enter a range accordingly";
                        die;
                    } else {
                        showData($finalData);
                        echo "else";
                    }
                }
            }
            ?>

        </div>
    </body>
</html>